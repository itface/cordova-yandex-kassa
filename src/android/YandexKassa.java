package ru.dolmatov.yandexkassa;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ru.yandex.money.android.sdk.Checkout;
import ru.yandex.money.android.sdk.PaymentParameters;
import ru.yandex.money.android.sdk.Amount;
import ru.yandex.money.android.sdk.PaymentMethodType;
import ru.yandex.money.android.sdk.TestParameters;
import ru.yandex.money.android.sdk.MockConfiguration;
import ru.yandex.money.android.sdk.TokenizationResult;

import java.util.Currency;
import java.math.BigDecimal;
import android.content.Intent;
import android.content.Context;
import android.app.Activity;
import java.util.HashSet;
import java.util.Set;

/**
 * This class echoes a string called from JavaScript.
 */
public class YandexKassa extends CordovaPlugin {

    private String action;
    private JSONArray args;
    private CallbackContext callbackContext;

    private static final int REQUEST_CODE_TOKENIZE = 33;
    
    private static final int YANDEX_MONEY   = 0b00001;
    private static final int BANK_CARD      = 0b00010;
    private static final int SBERBANK       = 0b00100;
    private static final int GOOGLE_PAY     = 0b01000;
    private static final int APPLE_PAY      = 0b10000;

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {

        this.action             = action;
        this.args               = args;
        this.callbackContext 	= callbackContext;

        try{
            switch(action){
                case "checkout":
                    this.checkout();
                    break;
                case "secure":
                    this.secure();
                    break;
                default:
                    return false;
            }
        }catch(Throwable e){
            callbackContext.error(e.getMessage());
        }

        return true;
    }

    private void checkout() throws JSONException {
        
        BigDecimal total    = BigDecimal.valueOf(args.getInt(0)).movePointLeft(2);
        Currency currency   = Currency.getInstance(args.getString(1)); 

        Amount amount               = new Amount(total, currency);
        String name                 = args.getString(2);
        String description          = args.getString(3);
        String clientApplicationKey = args.getString(4);
        String shopId               = args.getString(5);
        int mask                    = args.getInt(6);
        Boolean debug               = args.getBoolean(7);


        final Set<PaymentMethodType> paymentMethodTypes = getPaymentMethodTypes(mask);

        PaymentParameters paymentParameters = new PaymentParameters(amount, name, description, clientApplicationKey, shopId, paymentMethodTypes);

        Context context = this.cordova.getActivity().getApplicationContext();

        Intent intent;

        if(debug){
            final MockConfiguration mockConfiguration = new MockConfiguration(false, true, 5);
            final TestParameters testParameters = new TestParameters(true, true, mockConfiguration);
            intent = Checkout.createTokenizeIntent(context, paymentParameters, testParameters);
        }else{
            intent = Checkout.createTokenizeIntent(context, paymentParameters);   
        }

        cordova.startActivityForResult(this, intent, REQUEST_CODE_TOKENIZE);
    }

    private void secure() throws JSONException {
        Context context = this.cordova.getActivity().getApplicationContext();
        String url      = args.getString(0);
        Intent intent =  Checkout.create3dsIntent(context, url);
        cordova.startActivityForResult(this, intent, 1);
    }

    
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(action.equals("checkout") && requestCode == REQUEST_CODE_TOKENIZE){
            switch (resultCode) {
                case Activity.RESULT_OK:
                    TokenizationResult result = Checkout.createTokenizationResult(data);
                    callbackContext.success(result.getPaymentToken());
                    break;
                case Activity.RESULT_CANCELED:
                // user canceled tokenization
                    callbackContext.success("");
                    break;
            }
        }else if(action.equals("secure") && requestCode == 1){
            switch (resultCode) {
                case Activity.RESULT_OK:
                    callbackContext.success("true");
                    break;
                case Activity.RESULT_CANCELED:
                // user canceled tokenization
                    callbackContext.success("");
                    break;
                case Checkout.RESULT_ERROR:
                    callbackContext.error(data.getStringExtra(Checkout.EXTRA_ERROR_DESCRIPTION));
                    break;
            }

        }else{
            callbackContext.error("Bad request code");
        }
    }

    private static Set<PaymentMethodType> getPaymentMethodTypes(int mask) {
        final Set<PaymentMethodType> paymentMethodTypes = new HashSet<>();

        if ((YANDEX_MONEY & mask) == YANDEX_MONEY) {
            paymentMethodTypes.add(PaymentMethodType.YANDEX_MONEY);
        }

        if ((BANK_CARD & mask) == BANK_CARD) { 
            paymentMethodTypes.add(PaymentMethodType.BANK_CARD);
        }

        if ((SBERBANK & mask) == SBERBANK) {
            paymentMethodTypes.add(PaymentMethodType.SBERBANK);
        }

        if ((GOOGLE_PAY & mask) == GOOGLE_PAY) {
            paymentMethodTypes.add(PaymentMethodType.GOOGLE_PAY);
        }

        return paymentMethodTypes;
    }
}
