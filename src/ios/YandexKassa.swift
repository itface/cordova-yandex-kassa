import YandexCheckoutPayments
import YandexCheckoutPaymentsApi

@objc(YandexKassa)
class YandexKassa : CDVPlugin, TokenizationModuleOutput
{
	var commandId:String?
	var vc: (UIViewController & TokenizationModuleInput)?

    @objc
    func checkout(_ command: CDVInvokedUrlCommand)
    {
        self.commandId = command.callbackId

        let tokenizationModuleInputData  = TokenizationModuleInputData(
            clientApplicationKey: command.arguments[4] as! String,
            shopName: command.arguments[2] as! String,
            purchaseDescription: command.arguments[3] as! String,
            amount: Amount(value: Decimal(command.arguments[0] as! Int / 100), currency: .rub),
            tokenizationSettings: TokenizationSettings(paymentMethodTypes: self.getPaymentMethods(value: command.arguments[6] as! Int)),
            applePayMerchantIdentifier: (command.arguments[8] as! String)
        );

        let inputData: TokenizationFlow = .tokenization(tokenizationModuleInputData);

        self.vc = TokenizationAssembly.makeModule(inputData: inputData, moduleOutput: self)

        self.showModal();
    }

    @objc
    func secure(_ command: CDVInvokedUrlCommand)
    {
        self.commandId = command.callbackId
        self.vc!.start3dsProcess(requestUrl: command.arguments[0] as! String)

        self.showModal();
    }

    func getPaymentMethods(value: Int) -> PaymentMethodTypes
    {
        var paymentMethodTypes: PaymentMethodTypes = []
        /*
        if value & 0b00001 {
            paymentMethodTypes.insert(.yandexMoney)
        }
        */

        if value & 0b00100 > 0 {
            paymentMethodTypes.insert(.sberbank)
        }

        if value & 0b00010 > 0{
            paymentMethodTypes.insert(.bankCard)
        }

        if value & 0b10000 > 0{
            paymentMethodTypes.insert(.applePay)
        }

        return paymentMethodTypes
    }

    func didFinish(on module: TokenizationModuleInput, with error: YandexCheckoutPaymentsError?)
    {
        self.sendResult(message: "")
    }


    func didSuccessfullyPassedCardSec(on module: TokenizationModuleInput)
    {
		self.sendResult(message: "true")
    }

    func tokenizationModule(_ module: TokenizationModuleInput, didTokenize token: Tokens, paymentMethodType: PaymentMethodType)
    {
		self.sendResult(message: token.paymentToken)
    }

	func sendResult(message: String)
	{

        DispatchQueue.main.async{
            self.viewController.dismiss(animated: true, completion: nil)
        }


		self.commandDelegate!.send(CDVPluginResult(status: CDVCommandStatus_OK, messageAs: message), callbackId: self.commandId)
	}

	func showModal()
	{
		 DispatchQueue.main.async{
           self.viewController.present(self.vc!, animated: true, completion: nil)
        }
	}
}